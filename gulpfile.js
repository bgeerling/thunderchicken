var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var cleanCSS = require('gulp-clean-css');
var rename = require("gulp-rename");
var uglify = require('gulp-uglify');

// Copy third party libraries from /node_modules into /vendor
gulp.task('vendor', function(done) {


  // Bootstrap
  gulp.src([
      './node_modules/bootstrap/dist/css/bootstrap.min.css'
    ])
    .pipe(gulp.dest('./vendor'))

  // Font Awesome
  gulp.src([
    './node_modules/font-awesome/css/font-awesome.min.css',
  ])
  .pipe(gulp.dest('./vendor/font-awesome/css'))

  gulp.src([
      './node_modules/font-awesome/fonts/*'
    ])
    .pipe(gulp.dest('./vendor/font-awesome/fonts'))

  // jQuery
  gulp.src([
      './node_modules/jquery/dist/jquery.min.js'
    ])
    .pipe(gulp.dest('./vendor'))

  // jQuery Easing
  gulp.src([
      './node_modules/jquery.easing/jquery.easing.min.js'
    ])
    .pipe(gulp.dest('./vendor'))
  
  done();
});

// Compile SCSS
gulp.task('css:compile', function() {
  return gulp.src('./scss/**/*.scss')
    .pipe(sass.sync({
      outputStyle: 'expanded'
    }).on('error', sass.logError))
    .pipe(gulp.dest('./css'))
});

// Minify CSS
gulp.task('css:minify', gulp.series(['css:compile'], function() {
  return gulp.src([
      './css/*.css',
      '!./css/*.min.css'
    ])
    .pipe(cleanCSS())
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest('./css'))
    .pipe(browserSync.stream());
}));

// CSS
gulp.task('css', gulp.series(['css:compile', 'css:minify']));

// Minify JavaScript
gulp.task('js:minify', function() {
  return gulp.src([
      './js/*.js',
      '!./js/*.min.js'
    ])
    .pipe(uglify())
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest('./js'))
    .pipe(browserSync.stream());
});

// JS
gulp.task('js', gulp.series(['js:minify']));

// Default task
gulp.task('default', gulp.series(['css', 'js', 'vendor']));

// Configure the browserSync task
gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: "./"
    }
  });
});

// Dev task
gulp.task('dev', gulp.series(['css', 'js', 'browserSync'], function() {
  gulp.watch('./scss/*.scss', ['css']);
  gulp.watch('./js/*.js', ['js']);
  gulp.watch('./*.html', browserSync.reload);
}));

// Build task
gulp.task('build', gulp.series(['css', 'js', 'vendor'], function(done) {
  gulp.src(['./css/**.min.css']).pipe(gulp.dest('./dist/css'));
  gulp.src(['./js/stylish-portfolio.min.js']).pipe(gulp.dest('./dist/js'));
  gulp.src(['./img/**/*']).pipe(gulp.dest('./dist/img'));
  gulp.src(['./vendor/**/*']).pipe(gulp.dest('./dist/vendor'));
  gulp.src(['./**.html']).pipe(gulp.dest('./dist'));
  // gulp.src(['sitemap.xml']).pipe(gulp.dest('./dist'));
  // gulp.src(['robots.txt']).pipe(gulp.dest('./dist'));
  gulp.src(['_redirects']).pipe(gulp.dest('./dist'));
  gulp.src(['favicon.png']).pipe(gulp.dest('./dist'));
  done();
}));
